import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class DavililloPicassoTest {

	@Test
	void test() {
		int a1 = 1;
		int a2 = 1;
		int b1 = 2;
		int b2 = 2;
		int area = 1;
		assertEquals(area, DavililloPicasso.problema(a1, a2, b1, b2));
		
		a1 = 0;
		a2 = 0;
		b1 = 0;
		b2 = 1;
		area = 0;
		assertEquals(area, DavililloPicasso.problema(a1, a2, b1, b2));
		
		a1 = 1;
		a2 = 1;
		b1 = 1;
		b2 = 2;
		area = 0;
		assertEquals(area, DavililloPicasso.problema(a1, a2, b1, b2));
		
		a1 = 4;
		a2 = 7;
		b1 = 2;
		b2 = 8;
		area = 2;
		assertEquals(area, DavililloPicasso.problema(a1, a2, b1, b2));
		
		a1 = 6;
		a2 = 1;
		b1 = 7;
		b2 = 9;
		area = 8;
		assertEquals(area, DavililloPicasso.problema(a1, a2, b1, b2));
		
		a1 = 1;
		a2 = 1;
		b1 = 5;
		b2 = 7;
		area = 24;
		assertEquals(area, DavililloPicasso.problema(a1, a2, b1, b2));
		
		a1 = 8;
		a2 = 1;
		b1 = 3;
		b2 = 2;
		area = 5;
		assertEquals(area, DavililloPicasso.problema(a1, a2, b1, b2));
		
		a1 = 9;
		a2 = 4;
		b1 = 7;
		b2 = 7;
		area = 6;
		assertEquals(area, DavililloPicasso.problema(a1, a2, b1, b2));
		
		a1 = 5;
		a2 = 5;
		b1 = 7;
		b2 = 1;
		area = 8;
		assertEquals(area, DavililloPicasso.problema(a1, a2, b1, b2));
		
		a1 = 7;
		a2 = 2;
		b1 = 9;
		b2 = 3;
		area = 2;
		assertEquals(area, DavililloPicasso.problema(a1, a2, b1, b2));
		
		a1 = 0;
		a2 = 1;
		b1 = 0;
		b2 = 2;
		area = 0;
		assertEquals(area, DavililloPicasso.problema(a1, a2, b1, b2));
		
		a1 = 7;
		a2 = 8;
		b1 = 8;
		b2 = 7;
		area = 1;
		assertEquals(area, DavililloPicasso.problema(a1, a2, b1, b2));
		
		a1 = 0;
		a2 = 0;
		b1 = 0;
		b2 = 0;
		area = 0;
		assertEquals(area, DavililloPicasso.problema(a1, a2, b1, b2));
		
		a1 = 9;
		a2 = 9;
		b1 = 9;
		b2 = 9;
		area = 0;
		assertEquals(area, DavililloPicasso.problema(a1, a2, b1, b2));
		
		
		
	}

}
